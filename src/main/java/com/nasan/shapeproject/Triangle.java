/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class Triangle {
    private double b;
    private double h;
    public Triangle (double b,double h){
        this.b = b;
        this.h = h;
    }
    public double calTriArea (){
        return (b*h)/2;
    }
    public void setB (double b){
        if (b <= 0){
            System.out.println("Error: base must more than zero!!!!");
            return;
        }
        this.b = b;
    }
    public void setH (double h){
        if (h <= 0){
            System.out.println("Error: height must more than zero!!!!");
            return;
        }
        this.h = h;
    }
    public double getB (){
        return b;
    }
    public double getH (){
        return h;
    }
    
    @Override
    public String toString(){
        return "Area of triangle1 (b = " + this.getB() + ") ( h = " + this.getH()+") is " + this.calTriArea();
    }
}
