/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(3);
        System.out.println("Area of square1 (s = " + square1.getS() + ") is " + square1.calSquareArea());
        square1.setS(2);
        System.out.println("Area of square1 (s = " + square1.getS() + ") is " + square1.calSquareArea());
        square1.setS(0);
        System.out.println("Area of square1 (s = " + square1.getS() + ") is " + square1.calSquareArea());
        System.out.println(square1.toString());
        System.out.println(square1);
    }
}
