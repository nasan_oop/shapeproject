/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class Rectangle {
    private double w;
    private double h;
    public Rectangle (double w,double h){
        this.w = w;
        this.h = h;
    }
    public double calRecArea(){
        return w*h;
    }
    public void setW (double w){
        if(w <= 0){
            System.out.println("Error: width must more than zero!!!!");
            return;
        }
        this.w = w;
    }
    public void setH (double h){
        if(h <= 0){
            System.out.println("Error: height must more than zero!!!!");
            return;
        }
        this.h = h;
    }
    public double getW (){
        return w;
    }
    public double getH (){
        return h;
    }
    
    @Override
    public String toString(){
        return "Area of rectangle1 (w = " + this.getW() + ") ( h = " + this.getH()+") is " + this.calRecArea();
    }
}
