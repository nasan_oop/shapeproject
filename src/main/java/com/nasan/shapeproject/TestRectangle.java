/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,2);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + ") ( h = " + rectangle1.getH()+") is " + rectangle1.calRecArea());
        rectangle1.setW(5);
        rectangle1.setH(3);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + ") ( h = " + rectangle1.getH()+") is " + rectangle1.calRecArea());
        rectangle1.setW(0);
        rectangle1.setH(3);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + ") ( h = " + rectangle1.getH()+") is " + rectangle1.calRecArea());
        rectangle1.setW(5);
        rectangle1.setH(0);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + ") ( h = " + rectangle1.getH()+") is " + rectangle1.calRecArea());
        System.out.println(rectangle1.toString());
        System.out.println(rectangle1);
    }
}
