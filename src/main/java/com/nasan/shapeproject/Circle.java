/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class Circle {
    private double r; //create this variable for constructor mathod.
    public static final double pi = 22.0/7; //create value of pi by using static; "static" mean this pi is embed is this class,So every method can be use.If not using static you still can use this pi too but every method will have to pull and remember value of pi.
    public Circle (double r){ // constructor method.
        this.r = r; //recive value of r.
    }
    public double calArea(){
        return pi*Math.pow(r,2);
    }
    public double getR(){//This method is for get value of r in other class.
        return r;
    }
    public void setR(double r){ //This method will protect value of r for not to be zero or less,Every time you have to change value of r you need to use this method by using "objectName".setR .Because variable r is private that mean value of r can be change by method in this class.Ex if you want to change value of r(in circle1) to 99 in main method you have to use circle1.setR(99) 
        if(r <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return ;
        }
        this.r = r;//normal case
    }
    
    @Override
    public String toString(){
        return "Area of circle1 (r = " + this.getR() + ") is " + this.calArea();
    }
}
