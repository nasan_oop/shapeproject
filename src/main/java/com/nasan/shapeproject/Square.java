/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeproject;

/**
 *
 * @author nasan
 */
public class Square {
    private double s;
    public Square (double s){ 
        this.s = s; 
    }
    public double calSquareArea(){
        return s*s;
    }
    public void setS(double s){
        if(s <= 0){
            System.out.println("Error: Length must more than zero!!!!");
            return;
        }
        this.s = s;
    }
    public double getS (){
        return s;
    }
    @Override
    public String toString(){
        return "Area of square1 (s = " + this.getS() + ") is " + this.calSquareArea();
    }
    
}
